package main

func appendDeps(deps []string, pkg string, packageMap map[string][]string, depSet map[string]struct{}) []string {
	for _, dep := range packageMap[pkg] {
		if _, exists := depSet[dep]; !exists {
			if _, exists := packageMap[dep]; !exists {
				panic("Missing dep!")
			}
			deps = append(deps, dep)
			depSet[dep] = struct{}{}
			deps = appendDeps(deps, dep, packageMap, depSet)
		}
	}
	return deps
}

func DoTransitiveDeps(dconf []DebConfig) {
	packageMap := map[string][]string{}
	for _, dc := range dconf {
		packageMap[dc.Name] = dc.Deps
	}
	for i := range dconf {
		dc := &dconf[i]
		depSet := map[string]struct{}{}
		for _, dep := range dc.Deps {
			depSet[dep] = struct{}{}
		}

		deps := dc.Deps
		for _, dep := range dc.Deps {
			if _, exists := packageMap[dep]; !exists {
				panic("Missing dep!")
			}
			deps = appendDeps(deps, dep, packageMap, depSet)
		}
		dc.Deps = deps
	}
}
