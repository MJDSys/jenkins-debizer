package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"
)

const (
	url = "http://localhost:8080/"
)

func main() {
	f, err := os.Open("config")
	if err != nil {
		log.Fatalf("Failed to open file (%s)", err)
	}
	fBytes, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatalf("Failed to read file (%s)", err)
	}
	var dconf []DebConfig
	if err = yaml.Unmarshal(fBytes, &dconf); err != nil {
		log.Fatalf("Failed to parse yaml (%s)", err)
	}
	DoTransitiveDeps(dconf)
	configs, err := Generate(dconf)
	if err != nil {
		log.Fatalf("Failed to create config.xml files", err)
	}
	client := http.Client{}
	for name, cfg := range configs {
		cfgBuf := bytes.NewBufferString(cfg)
		req, err := http.NewRequest("POST", url+"createItem?name="+name, cfgBuf)
		if err != nil {
			log.Fatal("Failed to create creation req ", err)
		}
		req.Header.Set("Content-Type", "application/xml")
		resp, err := client.Do(req)
		if err != nil {
			log.Fatal("Failed to submit creation req ", err)
		}
		if resp.StatusCode == 200 {
			log.Print("Created config ", name)
			continue
		}

		// Update instead!
		cfgBuf = bytes.NewBufferString(cfg)
		req, err = http.NewRequest("POST", url+"job/"+name+"/config.xml", cfgBuf)
		if err != nil {
			log.Fatal("Failed to create update req ", err)
		}
		req.Header.Set("Content-Type", "application/xml")
		resp, err = client.Do(req)
		if err != nil {
			log.Fatal("Failed to submit update req ", err)
		}
		if resp.StatusCode == 200 {
			log.Print("Update config ", name)
		} else {
			log.Print("Failed to deal with config ", name)
		}
	}
}
