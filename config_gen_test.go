package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gopkg.in/yaml.v2"
)

func TestConfigGenerator(t *testing.T) {
	Convey("Starting with a config object", t, func() {
		cfg := []DebConfig{
			{
				Name: "dh-golang",
				Repo: "https://anonscm.debian.org/git/collab-maint/dh-golang.git",
			},
			{
				Name: "golang-codegangsta-cli",
				Repo: "git://anonscm.debian.org/pkg-go/packages/golang-codegangsta-cli.git",
				Deps: []string{
					"dh-golang",
				},
			},
			{
				Name: "golang-github-boltdb-bolt",
				Repo: "git://anonscm.debian.org/pkg-go/packages/golang-github-boltdb-bolt.git",
				Deps: []string{
					"dh-golang",
					"golang-codegangsta-cli",
				},
			},
			{
				Name: "golang-github-jonboulle-clockwork",
				Repo: "git://anonscm.debian.org/pkg-go/packages/golang-github-jonboulle-clockwork.git",
				Deps: []string{
					"dh-golang",
				},
			},
			{
				Name: "etcd",
				Repo: "git://anonscm.debian.org/pkg-go/packages/etcd.git",
				Deps: []string{
					"dh-golang",
					"golang-codegangsta-cli",
					"golang-github-boltdb-bolt",
					"golang-github-jonboulle-clockwork",
				},
			},
		}
		Convey("Generating config.xml files should be fine", func() {
			cfgs, err := Generate(cfg)
			So(err, ShouldBeNil)
			Convey("And give 5 configs", func() {
				So(len(cfgs), ShouldEqual, 5)
				Convey("One being for dh-golang, matching as expected", func() {
					So(cfgs["dh-golang"], ShouldEqual, dhGolangConfig)
				})
				Convey("One being for cli, matching as expected", func() {
					So(cfgs["golang-codegangsta-cli"], ShouldEqual, cliConfig)
				})
				Convey("One being for boltdb, matching as expected", func() {
					So(cfgs["golang-github-boltdb-bolt"], ShouldEqual, boltdbConfig)
				})
				Convey("One being for clockwork, matching as expected", func() {
					So(cfgs["golang-github-jonboulle-clockwork"], ShouldEqual, clockworkConfig)
				})
				Convey("One being for etc, matching as expected", func() {
					So(cfgs["etcd"], ShouldEqual, etcdConfig)
				})
			})
		})
	})
}

func TestYamlRead(t *testing.T) {
	Convey("Reading the data", t, func() {
		const str = `
- name: "dh-golang"
  repo: https://anonscm.debian.org/git/collab-maint/dh-golang.git
- name: "golang-github-boltdb-bolt"
  repo: "git://anonscm.debian.org/pkg-go/packages/golang-github-boltdb-bolt.git"
  deps:
    - "dh-golang"
    - "golang-codegangsta-cli"
`
		var in []DebConfig
		err := yaml.Unmarshal([]byte(str), &in)
		Convey("Should succeed", func() {
			So(err, ShouldBeNil)
			Convey("And match the expected input", func() {
				So(in, ShouldResemble, []DebConfig{
					{
						Name: "dh-golang",
						Repo: "https://anonscm.debian.org/git/collab-maint/dh-golang.git",
					},
					{
						Name: "golang-github-boltdb-bolt",
						Repo: "git://anonscm.debian.org/pkg-go/packages/golang-github-boltdb-bolt.git",
						Deps: []string{
							"dh-golang",
							"golang-codegangsta-cli",
						},
					},
				})
			})
		})
	})
}
