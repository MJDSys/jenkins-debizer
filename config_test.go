package main

import (
	"encoding/xml"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestConfigMarshal(t *testing.T) {
	Convey("Unmarshaling etcd test string should be fine", t, func() {
		v := JobConfig{}
		err := xml.Unmarshal([]byte(etcdConfig), &v)
		So(err, ShouldBeNil)
		Convey("And re marshaling should produce the same result", func() {
			b, err := xml.MarshalIndent(&v, "", "  ")
			So(err, ShouldBeNil)
			b = append([]byte(xml.Header), b...)
			So(string(b), ShouldEqual, etcdConfig)
		})
	})
	Convey("Unmarshaling dh-golang test string should be fine", t, func() {
		v := JobConfig{}
		err := xml.Unmarshal([]byte(dhGolangConfig), &v)
		So(err, ShouldBeNil)
		Convey("And re marshaling should produce the same result", func() {
			b, err := xml.MarshalIndent(&v, "", "  ")
			So(err, ShouldBeNil)
			b = append([]byte(xml.Header), b...)
			So(string(b), ShouldEqual, dhGolangConfig)
		})
	})
	Convey("Unmarshaling cli test string should be fine", t, func() {
		v := JobConfig{}
		err := xml.Unmarshal([]byte(cliConfig), &v)
		So(err, ShouldBeNil)
		Convey("And re marshaling should produce the same result", func() {
			b, err := xml.MarshalIndent(&v, "", "  ")
			So(err, ShouldBeNil)
			b = append([]byte(xml.Header), b...)
			So(string(b), ShouldEqual, cliConfig)
		})
	})
	Convey("Unmarshaling clockwork test string should be fine", t, func() {
		v := JobConfig{}
		err := xml.Unmarshal([]byte(clockworkConfig), &v)
		So(err, ShouldBeNil)
		Convey("And re marshaling should produce the same result", func() {
			b, err := xml.MarshalIndent(&v, "", "  ")
			So(err, ShouldBeNil)
			b = append([]byte(xml.Header), b...)
			So(string(b), ShouldEqual, clockworkConfig)
		})
	})
	Convey("Unmarshaling boltdb test string should be fine", t, func() {
		v := JobConfig{}
		err := xml.Unmarshal([]byte(boltdbConfig), &v)
		So(err, ShouldBeNil)
		Convey("And re marshaling should produce the same result", func() {
			b, err := xml.MarshalIndent(&v, "", "  ")
			So(err, ShouldBeNil)
			b = append([]byte(xml.Header), b...)
			So(string(b), ShouldEqual, boltdbConfig)
		})
	})
}

const etcdConfig = `<?xml version="1.0" encoding="UTF-8"?>
<project>
  <actions></actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties></properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.4.0">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <name>origin</name>
        <refspec>+refs/*:refs/remotes/origin/*</refspec>
        <url>git://anonscm.debian.org/pkg-go/packages/etcd.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/tags/debian/*</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"></submoduleCfg>
    <extensions></extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>true</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>true</blockBuildWhenUpstreamBuilding>
  <triggers>
    <jenkins.triggers.ReverseBuildTrigger>
      <spec></spec>
      <upstreamProjects>dh-golang,golang-codegangsta-cli,golang-github-boltdb-bolt,golang-github-jonboulle-clockwork,</upstreamProjects>
      <threshold>
        <name>SUCCESS</name>
        <ordinal>0</ordinal>
        <color>BLUE</color>
        <completeBuild>true</completeBuild>
      </threshold>
    </jenkins.triggers.ReverseBuildTrigger>
    <hudson.triggers.SCMTrigger>
      <spec>H * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>dh-golang</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>golang-codegangsta-cli</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>golang-github-boltdb-bolt</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>golang-github-jonboulle-clockwork</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <org.jenkinsci.plugins.managedscripts.ScriptBuildStep plugin="managed-scripts@1.2.1">
      <buildStepId>org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208</buildStepId>
      <tokenized>false</tokenized>
    </org.jenkinsci.plugins.managedscripts.ScriptBuildStep>
  </builders>
  <publishers>
    <hudson.tasks.ArtifactArchiver>
      <artifacts>output-deb/*.deb</artifacts>
      <allowEmptyArchive>false</allowEmptyArchive>
      <onlyIfSuccessful>true</onlyIfSuccessful>
      <fingerprint>false</fingerprint>
      <defaultExcludes>true</defaultExcludes>
    </hudson.tasks.ArtifactArchiver>
  </publishers>
  <buildWrappers></buildWrappers>
</project>`

const dhGolangConfig = `<?xml version="1.0" encoding="UTF-8"?>
<project>
  <actions></actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties></properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.4.0">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <name>origin</name>
        <refspec>+refs/*:refs/remotes/origin/*</refspec>
        <url>https://anonscm.debian.org/git/collab-maint/dh-golang.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/tags/debian/*</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"></submoduleCfg>
    <extensions></extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>true</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>true</blockBuildWhenUpstreamBuilding>
  <triggers>
    <hudson.triggers.SCMTrigger>
      <spec>H * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <org.jenkinsci.plugins.managedscripts.ScriptBuildStep plugin="managed-scripts@1.2.1">
      <buildStepId>org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208</buildStepId>
      <tokenized>false</tokenized>
    </org.jenkinsci.plugins.managedscripts.ScriptBuildStep>
  </builders>
  <publishers>
    <hudson.tasks.ArtifactArchiver>
      <artifacts>output-deb/*.deb</artifacts>
      <allowEmptyArchive>false</allowEmptyArchive>
      <onlyIfSuccessful>true</onlyIfSuccessful>
      <fingerprint>false</fingerprint>
      <defaultExcludes>true</defaultExcludes>
    </hudson.tasks.ArtifactArchiver>
  </publishers>
  <buildWrappers></buildWrappers>
</project>`

const cliConfig = `<?xml version="1.0" encoding="UTF-8"?>
<project>
  <actions></actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties></properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.4.0">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <name>origin</name>
        <refspec>+refs/*:refs/remotes/origin/*</refspec>
        <url>git://anonscm.debian.org/pkg-go/packages/golang-codegangsta-cli.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/tags/debian/*</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"></submoduleCfg>
    <extensions></extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>true</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>true</blockBuildWhenUpstreamBuilding>
  <triggers>
    <jenkins.triggers.ReverseBuildTrigger>
      <spec></spec>
      <upstreamProjects>dh-golang,</upstreamProjects>
      <threshold>
        <name>SUCCESS</name>
        <ordinal>0</ordinal>
        <color>BLUE</color>
        <completeBuild>true</completeBuild>
      </threshold>
    </jenkins.triggers.ReverseBuildTrigger>
    <hudson.triggers.SCMTrigger>
      <spec>H * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>dh-golang</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <org.jenkinsci.plugins.managedscripts.ScriptBuildStep plugin="managed-scripts@1.2.1">
      <buildStepId>org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208</buildStepId>
      <tokenized>false</tokenized>
    </org.jenkinsci.plugins.managedscripts.ScriptBuildStep>
  </builders>
  <publishers>
    <hudson.tasks.ArtifactArchiver>
      <artifacts>output-deb/*.deb</artifacts>
      <allowEmptyArchive>false</allowEmptyArchive>
      <onlyIfSuccessful>true</onlyIfSuccessful>
      <fingerprint>false</fingerprint>
      <defaultExcludes>true</defaultExcludes>
    </hudson.tasks.ArtifactArchiver>
  </publishers>
  <buildWrappers></buildWrappers>
</project>`

const boltdbConfig = `<?xml version="1.0" encoding="UTF-8"?>
<project>
  <actions></actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties></properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.4.0">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <name>origin</name>
        <refspec>+refs/*:refs/remotes/origin/*</refspec>
        <url>git://anonscm.debian.org/pkg-go/packages/golang-github-boltdb-bolt.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/tags/debian/*</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"></submoduleCfg>
    <extensions></extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>true</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>true</blockBuildWhenUpstreamBuilding>
  <triggers>
    <jenkins.triggers.ReverseBuildTrigger>
      <spec></spec>
      <upstreamProjects>dh-golang,golang-codegangsta-cli,</upstreamProjects>
      <threshold>
        <name>SUCCESS</name>
        <ordinal>0</ordinal>
        <color>BLUE</color>
        <completeBuild>true</completeBuild>
      </threshold>
    </jenkins.triggers.ReverseBuildTrigger>
    <hudson.triggers.SCMTrigger>
      <spec>H * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>dh-golang</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>golang-codegangsta-cli</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <org.jenkinsci.plugins.managedscripts.ScriptBuildStep plugin="managed-scripts@1.2.1">
      <buildStepId>org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208</buildStepId>
      <tokenized>false</tokenized>
    </org.jenkinsci.plugins.managedscripts.ScriptBuildStep>
  </builders>
  <publishers>
    <hudson.tasks.ArtifactArchiver>
      <artifacts>output-deb/*.deb</artifacts>
      <allowEmptyArchive>false</allowEmptyArchive>
      <onlyIfSuccessful>true</onlyIfSuccessful>
      <fingerprint>false</fingerprint>
      <defaultExcludes>true</defaultExcludes>
    </hudson.tasks.ArtifactArchiver>
  </publishers>
  <buildWrappers></buildWrappers>
</project>`

const clockworkConfig = `<?xml version="1.0" encoding="UTF-8"?>
<project>
  <actions></actions>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties></properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.4.0">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <name>origin</name>
        <refspec>+refs/*:refs/remotes/origin/*</refspec>
        <url>git://anonscm.debian.org/pkg-go/packages/golang-github-jonboulle-clockwork.git</url>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/tags/debian/*</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"></submoduleCfg>
    <extensions></extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>true</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>true</blockBuildWhenUpstreamBuilding>
  <triggers>
    <jenkins.triggers.ReverseBuildTrigger>
      <spec></spec>
      <upstreamProjects>dh-golang,</upstreamProjects>
      <threshold>
        <name>SUCCESS</name>
        <ordinal>0</ordinal>
        <color>BLUE</color>
        <completeBuild>true</completeBuild>
      </threshold>
    </jenkins.triggers.ReverseBuildTrigger>
    <hudson.triggers.SCMTrigger>
      <spec>H * * * *</spec>
      <ignorePostCommitHooks>false</ignorePostCommitHooks>
    </hudson.triggers.SCMTrigger>
  </triggers>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.plugins.copyartifact.CopyArtifact plugin="copyartifact@1.36.1">
      <project>dh-golang</project>
      <filter></filter>
      <target>deb-deps</target>
      <excludes></excludes>
      <selector class="hudson.plugins.copyartifact.StatusBuildSelector"></selector>
      <flatten>true</flatten>
      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>
    </hudson.plugins.copyartifact.CopyArtifact>
    <org.jenkinsci.plugins.managedscripts.ScriptBuildStep plugin="managed-scripts@1.2.1">
      <buildStepId>org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208</buildStepId>
      <tokenized>false</tokenized>
    </org.jenkinsci.plugins.managedscripts.ScriptBuildStep>
  </builders>
  <publishers>
    <hudson.tasks.ArtifactArchiver>
      <artifacts>output-deb/*.deb</artifacts>
      <allowEmptyArchive>false</allowEmptyArchive>
      <onlyIfSuccessful>true</onlyIfSuccessful>
      <fingerprint>false</fingerprint>
      <defaultExcludes>true</defaultExcludes>
    </hudson.tasks.ArtifactArchiver>
  </publishers>
  <buildWrappers></buildWrappers>
</project>`
