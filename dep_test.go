package main

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestDepSolver(t *testing.T) {
	Convey("With a starting package list, preform a resolve", t, func() {
		dconf := []DebConfig{
			DebConfig{
				Name: "a",
			},
			DebConfig{
				Name: "b",
				Deps: []string{"a", "c"},
			},
			DebConfig{
				Name: "c",
			},
			DebConfig{
				Name: "d",
				Deps: []string{"b", "f"},
			},
			DebConfig{
				Name: "e",
			},
			DebConfig{
				Name: "f",
				Deps: []string{"a", "e", "g"},
			},
			DebConfig{
				Name: "g",
				Deps: []string{"h"},
			},
			DebConfig{
				Name: "h",
			},
		}
		DoTransitiveDeps(dconf)
		Convey("Should match the expected out", func() {
			econf := []DebConfig{
				DebConfig{
					Name: "a",
				},
				DebConfig{
					Name: "b",
					Deps: []string{"a", "c"},
				},
				DebConfig{
					Name: "c",
				},
				DebConfig{
					Name: "d",
					Deps: []string{"b", "f", "a", "c", "e", "g", "h"},
				},
				DebConfig{
					Name: "e",
				},
				DebConfig{
					Name: "f",
					Deps: []string{"a", "e", "g", "h"},
				},
				DebConfig{
					Name: "g",
					Deps: []string{"h"},
				},
				DebConfig{
					Name: "h",
				},
			}
			So(dconf, ShouldResemble, econf)
			Convey("And again won't change anything", func() {
				DoTransitiveDeps(dconf)
				So(dconf, ShouldResemble, econf)
			})
		})
	})
	Convey("With a starting package list with missing deps, preform a resolve should panic", t, func() {
		dconf := []DebConfig{
			DebConfig{
				Name: "a",
				Deps: []string{"c"},
			},
		}
		So(func() { DoTransitiveDeps(dconf) }, ShouldPanic)
	})
}
