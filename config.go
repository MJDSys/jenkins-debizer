package main

import (
	"encoding/xml"
)

type JobConfig struct {
	XMLName xml.Name `xml:"project"`

	Actions          string `xml:"actions"`
	Description      string `xml:"description"`
	KeepDependencies bool   `xml:"keepDependencies"`
	Properties       string `xml:"properties"`

	Scm ScmConfig `xml:"scm"`

	CanRoam                          bool `xml:"canRoam"`
	Disabled                         bool `xml:"disabled"`
	BlockBuildWhenDownstreamBuilding bool `xml:"blockBuildWhenDownstreamBuilding"`
	BlockBuildWhenUpstreamBuilding   bool `xml:"blockBuildWhenUpstreamBuilding"`

	Triggers TriggersConfig `xml:"triggers"`

	ConcurrentBuild bool `xml:"concurrentBuild"`

	CopyBuilders     []CopyBuildersConfig   `xml:"builders>hudson.plugins.copyartifact.CopyArtifact"`
	ScriptBuilder    ScriptBuilderConfig    `xml:"builders>org.jenkinsci.plugins.managedscripts.ScriptBuildStep"`
	ArtifactArchiver ArtifactArchiverConfig `xml:"publishers>hudson.tasks.ArtifactArchiver"`

	BuildWrappers string `xml:"buildWrappers"`
}

type ScmConfig struct {
	Class  string `xml:"class,attr"`
	Plugin string `xml:"plugin,attr"`

	ConfigVersion     int          `xml:"configVersion"`
	UserRemoteConfigs RemoteConfig `xml:"userRemoteConfigs"`
	Branches          BranchConfig `xml:"branches"`

	DoGenerateSubmoduleConfigurations bool            `xml:"doGenerateSubmoduleConfigurations"`
	SubModuleCfg                      SubModuleConfig `xml:"submoduleCfg"`
	Extensions                        string          `xml:"extensions"`
}

type RemoteConfig struct {
	Name    string `xml:"hudson.plugins.git.UserRemoteConfig>name"`
	Refspec string `xml:"hudson.plugins.git.UserRemoteConfig>refspec"`
	Url     string `xml:"hudson.plugins.git.UserRemoteConfig>url"`
}

type BranchConfig struct {
	Name string `xml:"hudson.plugins.git.BranchSpec>name"`
}

type SubModuleConfig struct {
	Class string `xml:"class,attr"`
}

type TriggersConfig struct {
	ReverseBuildTrigger *DepConfig       `xml:"jenkins.triggers.ReverseBuildTrigger"`
	SCMTrigger          SCMTriggerConfig `xml:"hudson.triggers.SCMTrigger"`
}

type DepConfig struct {
	Spec                   string `xml:"spec"`
	UpstreamProjects       string `xml:"upstreamProjects"`
	ThresholdName          string `xml:"threshold>name"`
	ThresholdOrdinal       int    `xml:"threshold>ordinal"`
	ThresholdColor         string `xml:"threshold>color"`
	ThresholdCompleteBuild bool   `xml:"threshold>completeBuild"`
}

type SCMTriggerConfig struct {
	Spec                  string `xml:"spec"`
	IgnorePostCommitHooks bool   `xml:"ignorePostCommitHooks"`
}

type CopyBuildersConfig struct {
	Plugin string `xml:"plugin,attr"`

	Project  string `xml:"project"`
	Filter   string `xml:"filter"`
	Target   string `xml:"target"`
	Excludes string `xml:"excludes"`
	Selector struct {
		Class string `xml:"class,attr"`
	} `xml:"selector"`
	Flatten                   bool `xml:"flatten"`
	DoNotFingerprintArtifacts bool `xml:"doNotFingerprintArtifacts"`
}

type ScriptBuilderConfig struct {
	Plugin string `xml:"plugin,attr"`

	BuildStepId string `xml:"buildStepId"`
	Tokenized   bool   `xml:"tokenized"`
}

type ArtifactArchiverConfig struct {
	Artifacts        string `xml:"artifacts"`
	AllowEmptyArhive bool   `xml:"allowEmptyArchive"`
	OnlyIfSuccessful bool   `xml:"onlyIfSuccessful"`
	Fingerprint      bool   `xml:"fingerprint"`
	DefaultExcludes  bool   `xml:"defaultExcludes"`
}
