package main

import (
	"encoding/xml"
	"strings"
)

type DebConfig struct {
	Name string
	Repo string
	Deps []string
}

func Generate(cfgs []DebConfig) (map[string]string, error) {
	ret := make(map[string]string)
	for _, cfg := range cfgs {
		out := JobConfig{}

		out.Scm.Class = "hudson.plugins.git.GitSCM"
		out.Scm.Plugin = "git@2.4.0"
		out.Scm.ConfigVersion = 2

		out.Scm.UserRemoteConfigs.Name = "origin"
		out.Scm.UserRemoteConfigs.Refspec = "+refs/*:refs/remotes/origin/*"
		out.Scm.UserRemoteConfigs.Url = cfg.Repo

		out.Scm.Branches.Name = "*/tags/debian/*"
		out.Scm.SubModuleCfg.Class = "list"

		out.CanRoam = true
		out.BlockBuildWhenDownstreamBuilding = true
		out.BlockBuildWhenUpstreamBuilding = true

		out.Triggers.SCMTrigger.Spec = "H * * * *"
		if len(cfg.Deps) != 0 {
			out.Triggers.ReverseBuildTrigger = &DepConfig{
				UpstreamProjects:       strings.Join(cfg.Deps, ",") + ",",
				ThresholdName:          "SUCCESS",
				ThresholdOrdinal:       0,
				ThresholdColor:         "BLUE",
				ThresholdCompleteBuild: true,
			}
		}

		for _, dep := range cfg.Deps {
			cbc := CopyBuildersConfig{
				Plugin:  "copyartifact@1.36.1",
				Project: dep,
				Target:  "deb-deps",
				Flatten: true,
			}
			cbc.Selector.Class = "hudson.plugins.copyartifact.StatusBuildSelector"
			out.CopyBuilders = append(out.CopyBuilders, cbc)
		}

		out.ScriptBuilder.Plugin = "managed-scripts@1.2.1"
		out.ScriptBuilder.BuildStepId = "org.jenkinsci.plugins.managedscripts.ScriptConfig1444539026208"

		out.ArtifactArchiver.Artifacts = "output-deb/*.deb"
		out.ArtifactArchiver.OnlyIfSuccessful = true
		out.ArtifactArchiver.DefaultExcludes = true

		str, err := xml.MarshalIndent(out, "", "  ")
		if err != nil {
			return nil, err
		}
		ret[cfg.Name] = xml.Header + string(str)
	}
	return ret, nil
}
